let mongoose = require('mongoose');

// Article Schema
let articleSchema = mongoose.Schema({
  player_name: {
    type: String,
    required: true
  },
  bronze: {
    type: Number,
    required: false
  },
  silver: {
    type: Number,
    required: false
  },
  gold: {
    type: Number,
    required: false
  },
  detail: {
    type: String,
    required: false
  },
  author: {
    type: String,
    required: true
  },
  picture: {
    type: String,
    required: false
  }
});

let Article = module.exports = mongoose.model('Article', articleSchema);